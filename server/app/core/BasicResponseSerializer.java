package core;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class BasicResponseSerializer extends JsonSerializer<BasicResponse> {


    @Override
    public void serialize(BasicResponse basicResponse, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("code", basicResponse.getCode());
        jsonGenerator.writeStringField("status", basicResponse.getStatus());
        if(basicResponse.getMessage() != null) {
            jsonGenerator.writeStringField("message", basicResponse.getMessage());
        }
        if(basicResponse.getUniqueResult() != null){
            jsonGenerator.writeObjectField("data", basicResponse.getUniqueResult());
        }else{
            if(basicResponse.getData() != null) jsonGenerator.writeObjectField("data", basicResponse.getData());
        }
        if(basicResponse.getLinks() != null) jsonGenerator.writeObjectField("links", basicResponse.getLinks());
        jsonGenerator.writeEndObject();
    }
}
