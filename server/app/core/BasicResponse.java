package core;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(using = BasicResponseSerializer.class)
public class BasicResponse<T> {
    private long code;
    private String status;
    private String message;
    private List<T> data;
    private T uniqueResult;
    private List<String> links;

    public void setCode(long code) {
        this.code = code;
        if(code >= 500 && code <= 599){
            this.status = "fail";
        }
        if(code >= 400 && code <= 499){
            this.status = "error";
        }
        status = "success";
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<T> getData() {
        return data;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    public T getUniqueResult() {
        return uniqueResult;
    }

    public void setUniqueResult(T uniqueResult) {
        this.uniqueResult = uniqueResult;
    }
}
