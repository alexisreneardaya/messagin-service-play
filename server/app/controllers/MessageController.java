package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.BasicResponse;
import models.Message;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.List;

public class MessageController extends Controller {

    public static Result messages()throws JsonProcessingException {
        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue = mapper.convertValue("", JsonNode.class);
        int status = 0;

        try {
            List<Message> messages = Message.all();
            if(messages.size() == 0 || messages.equals(null)) {
                status=NO_CONTENT;
                basicResponse.setCode(NO_CONTENT);
                basicResponse.setMessage("There aren't messages to show");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }
            else {
                status=OK;
                basicResponse.setCode(OK);
                basicResponse.setData(messages);
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }

        } catch (Exception e) {
            status=INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);

    }

    public static Result sentMessage(String userOrigin,String userDestination)throws JsonProcessingException{
        String message= request().getQueryString("msg");
        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue = mapper.convertValue("", JsonNode.class);
        String verifyUserOrigin;
        String verifyUserDestination;
        Message newMessage= new Message();
        int status = 0;
        try {
            if (userOrigin == null || userOrigin == "" && userDestination==null || userOrigin=="") {
                status= BAD_REQUEST;
                basicResponse.setCode(BAD_REQUEST);
                basicResponse.setMessage("Could not sent message");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }
            verifyUserOrigin = User.getUserByUserName(userOrigin).getUserName();
            verifyUserDestination = User.getUserByUserName(userDestination).getUserName();
            if (verifyUserOrigin==null) {
                status= NOT_FOUND;
                basicResponse.setCode(NOT_FOUND);
                basicResponse.setMessage("User Origin not found. Can not sent message.");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }else{
                if(verifyUserDestination==null){
                    status= NOT_FOUND;
                    basicResponse.setCode(NOT_FOUND);
                    basicResponse.setMessage("User Destination not found. Can not sent message.");
                    convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                }else{
                    if(verifyUserOrigin.equals(userOrigin)){
                        if (verifyUserDestination.equals(userDestination)) {
                            newMessage.setIdUserOrigin(User.getUserByUserName(userOrigin));
                            newMessage.setIdUserDestination(User.getUserByUserName(userDestination));
                            newMessage.setDateSent(Message.registerDateMessage());
                            newMessage.setMessage(message);
                            newMessage.setState(false);
                            Message.create(newMessage);
                            status = CREATED;
                            basicResponse.setCode(CREATED);
                            basicResponse.setMessage("Message sent correctly.");
                            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                        }
                    }
                }
            }

        } catch (Exception e) {
            status= INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);
    }

    public static Result getMessageByUserName(String userName)throws  JsonProcessingException{
        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue;
        int status = 0;
        try {
            List<Message> messages = Message.getMessageByUserName(userName);
            if(messages.size() == 0 || messages.equals(null)) {
                status=NO_CONTENT;
                basicResponse.setCode(NO_CONTENT);
                basicResponse.setMessage("There aren't messages to show");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }
            else {
                for(Message message :messages ){
                    if(!message.isState()){
                        Message.update(message);
                    }
                }
                messages = Message.getMessageByUserName(userName);
                status=OK;
                basicResponse.setCode(OK);
                basicResponse.setData(messages);
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }
        } catch (Exception e) {
            status=INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);
    }
}
