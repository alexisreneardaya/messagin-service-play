package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.BasicResponse;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

public class UserController extends Controller {
    public static Result users() throws JsonProcessingException {
        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue = mapper.convertValue("", JsonNode.class);
        int status = 0;

        try {
            List<User> users = User.all();
            if(users.size() == 0 || users.equals(null)) {
                status=NO_CONTENT;
                basicResponse.setCode(NO_CONTENT);
                basicResponse.setMessage("There aren't users to show");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }
            else {
                status=OK;
                basicResponse.setCode(OK);
                basicResponse.setData(users);
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }

        } catch (Exception e) {
            status=INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);

    }

    public static Result getUserByUserName() throws JsonProcessingException {
        JsonNode node = request().body().asJson();
        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue = mapper.convertValue("", JsonNode.class);
        User newCar = mapper.treeToValue(node, User.class);
        User user= new User();
        int status = 0;

        try {
            user = User.getUserByUserName(newCar.getUserName());
            if (user.getId()==null) {
                status= NOT_FOUND;
                basicResponse.setCode(NOT_FOUND);
                basicResponse.setMessage("User not found.");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }else{
                status = FOUND;
                basicResponse.setCode(FOUND);
                basicResponse.setUniqueResult(user);
                basicResponse.setMessage("User found.");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }

        } catch (Exception e) {
            status=INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);

    }

    public static Result addUser(String userName) throws JsonProcessingException {
        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue=mapper.convertValue("", JsonNode.class);
        User newUser= new User();
        User userCreated= new User();
        String user;
        int status = 0;

        try {
            if (userName.equals(null) || userName.equals("")) {
                status= BAD_REQUEST;
                basicResponse.setCode(BAD_REQUEST);
                basicResponse.setMessage("Could not create user");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }
            user = User.getUserByUserName(userName).getUserName();
            if (user==null) {
                newUser.setUserName(userName);
                newUser.setState(true);
                User.create(newUser);
                userCreated = User.getUserByUserName(userName);
                status = CREATED;
                basicResponse.setCode(CREATED);
                basicResponse.setUniqueResult(userCreated);
                basicResponse.setMessage("User created correctly");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }else {
                if (user.equals(userName)) {
                    status = CONFLICT;
                    basicResponse.setCode(CONFLICT);
                    basicResponse.setMessage("User already exists");
                    convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                }
            }
        } catch (Exception e) {
            status= INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);
    }

    public static Result updateUser(Long id,String userName,Boolean state) throws JsonProcessingException {

        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue = mapper.convertValue("", JsonNode.class);
        User updateUser= new User();
        int status = 0;
        try {
            if (id == null && userName == null) {
                status= BAD_REQUEST;
                basicResponse.setCode(BAD_REQUEST);
                basicResponse.setMessage("Could not update user");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }

            List<User> users = User.all();
            for(User user : users) {
                if(user.getId()==id) {
                    updateUser.setId(id);
                    updateUser.setUserName(userName);
                    updateUser.setState(state);
                    User.update(updateUser);
                    status= OK;
                    basicResponse.setCode(OK);
                    basicResponse.setMessage("User modified correctly");
                    convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                    break;
                }else{
                    status= NOT_MODIFIED;
                    basicResponse.setCode(NOT_MODIFIED);
                    basicResponse.setMessage("User does not exist");
                    convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                    break;
                }
            }
        } catch (Exception e) {
            status= INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);
    }

    public static Result deleteUser(Long id) throws JsonProcessingException {

        BasicResponse basicResponse = new BasicResponse();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode convertValue = mapper.convertValue("", JsonNode.class);
        int status = 0;

        try {
            if (id == null) {
                status= BAD_REQUEST;
                basicResponse.setCode(BAD_REQUEST);
                basicResponse.setMessage("Could not delete user");
                convertValue = mapper.convertValue(basicResponse, JsonNode.class);
            }

            List<User> users = User.all();
            for(User user : users) {
                if(user.getId()==id) {
                    User.delete(id);
                    status= OK;
                    basicResponse.setCode(OK);
                    basicResponse.setMessage("User deleted correctly");
                    convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                    break;
                }else{
                    status= NOT_FOUND;
                    basicResponse.setCode(NOT_FOUND);
                    basicResponse.setMessage("User does not exist");
                    convertValue = mapper.convertValue(basicResponse, JsonNode.class);
                    break;
                }
            }
        } catch (Exception e) {
            status= INTERNAL_SERVER_ERROR;
            basicResponse.setCode(INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
            convertValue = mapper.convertValue(basicResponse, JsonNode.class);
        }

        return status(status,convertValue);
    }
}
