package models;

import java.util.*;
import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User extends Model {
    public Long id;
    public String userName;
    public boolean state;

    public User() {
    }

    public User(Long id, String userName, boolean state) {
        this.id = id;
        this.userName = userName;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_name", nullable = false)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "state", nullable = false)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public static Finder<Long, User> find = new Finder<Long, User>(
            Long.class, User.class
    );

    public static List<User> all() {
        return find.all();
    }

    public static User getUserById(Long id) {
        return find.byId(id);
    }

    public static User getUserByUserName(String userName) {
        User user = find.where().like("user_name",userName).findUnique();
        if(user!=null){
            return user;
        }else{
          return new User();
        }
    }

    public static void create(User user) {
        user.save();
    }

    public static void update(User user) {
        user.update();
    }

    public static void delete(Long id) {
        find.ref(id).delete();
    }

}
