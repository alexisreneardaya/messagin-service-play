package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name = "message")
public class Message extends Model {
    public long id;
    public String message;
    public User idUserOrigin;
    public User idUserDestination;
    public long dateSent;
    public long dateSeen;
    public boolean state;

    public Message() {
    }

    public Message(String message, User idUserOrigin, User idUserDestination, long dateSent, long dateSeen, boolean state) {
        this.message = message;
        this.idUserOrigin = idUserOrigin;
        this.idUserDestination = idUserDestination;
        this.dateSent = dateSent;
        this.dateSeen = dateSeen;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "content", nullable = false)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @ManyToOne
    @JoinColumn(name = "id_user_origin", referencedColumnName = "id", nullable = false)
    public User getIdUserOrigin() {
        return idUserOrigin;
    }

    public void setIdUserOrigin(User idUserOrigin) {
        this.idUserOrigin = idUserOrigin;
    }

    @ManyToOne
    @JoinColumn(name = "id_user_destination", referencedColumnName = "id", nullable = false)
    public User getIdUserDestination() {
        return idUserDestination;
    }

    public void setIdUserDestination(User idUserDestination) {
        this.idUserDestination = idUserDestination;
    }

    @Basic
    @Column(name = "date_sent", nullable = false)
    public long getDateSent() {
        return dateSent;
    }

    public void setDateSent(long dateSent) {
        this.dateSent = dateSent;
    }

    @Basic
    @Column(name = "date_seen", nullable = false)
    public long getDateSeen() {
        return dateSeen;
    }

    public void setDateSeen(long dateSeen) {
        this.dateSeen = dateSeen;
    }

    @Basic
    @Column(name = "state", nullable = false)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public static Model.Finder<Long, Message> find = new Model.Finder<Long, Message>(
            Long.class, Message.class
    );

    public static List<Message> all() {
        return find.all();
    }

    public static List<Message> getMessageByUserName(String userName) {
        User user= User.getUserByUserName(userName);
        List<Message> messages = find.where().eq("id_user_destination",user.getId()).findList();
        return messages;
    }

    public static void update(Message message) throws ParseException {
        Message updateMessage = Message.find.byId(message.getId());
        updateMessage.setDateSeen(registerDateMessage());
        updateMessage.setState(true);
        updateMessage.update();
    }

    public static void create(Message message) {
        message.save();
    }
    public static long registerDateMessage() throws ParseException {
        SimpleDateFormat formaterShortDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        formaterShortDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateNow = formaterShortDate.format((Calendar.getInstance().getTime()));
        Date dateSent= formaterShortDate.parse(dateNow);
        long time = dateSent.getTime()/1000;
        return time ;
    }
}
