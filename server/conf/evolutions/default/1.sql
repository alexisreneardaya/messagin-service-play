# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table message (
  id                        bigint auto_increment not null,
  content                   varchar(255) not null,
  id_user_origin            bigint not null,
  id_user_destination       bigint not null,
  date_sent                 bigint not null,
  date_seen                 bigint not null,
  state                     boolean,
  constraint pk_message primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  user_name                 varchar(255) not null,
  state                     boolean,
  constraint pk_user primary key (id))
;

alter table message add constraint fk_message_idUserOrigin_1 foreign key (id_user_origin) references user (id) on delete restrict on update restrict;
create index ix_message_idUserOrigin_1 on message (id_user_origin);
alter table message add constraint fk_message_idUserDestination_2 foreign key (id_user_destination) references user (id) on delete restrict on update restrict;
create index ix_message_idUserDestination_2 on message (id_user_destination);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists message;

drop table if exists user;

SET REFERENTIAL_INTEGRITY TRUE;

