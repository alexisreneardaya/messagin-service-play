;(function() {

  	angular.module("message-play").controller('LoginController', LoginController);

  	LoginController.$inject = ['$scope', '$state', 'resizeTemplate', 'AuthenticationService','status_code','$localStorage'];

  	function LoginController($scope, $state, resizeTemplate, AuthenticationService,status_code,$localStorage) {
        resizeTemplate.resize();
        $scope.user="";
        $scope.messageAlert="";
        $scope.userName="";
        $scope.login=login;

        function login() {
            $scope.alertError = false;
            AuthenticationService.Login($scope.userName, function (result) {
                try {
                    $scope.user = result;
                    if ($scope.user.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                    if ($scope.user.data.code == status_code.FOUND) {
                        $state.go('message');
                        $localStorage.currentUser = {
                            userId: $scope.user.data.data.id,
                            userName: $scope.user.data.data.userName
                        };

                    }
                    if ($scope.user.data.code == status_code.NOT_FOUND) {
                        $scope.messageAlert = "  Nombre de usuario incorrecto.";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                    if ($scope.user.data.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                    if ($scope.user.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                }catch (e){

                }
            });
        };

        function cleanInputs() {
            try{
                $scope.userName="";
            }catch(e){

            }
        }
  	}	

})();