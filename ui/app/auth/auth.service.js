(function () {
    'use strict';
 
    angular
        .module('message-play')
        .factory('AuthenticationService', AuthenticationService);
    
    AuthenticationService.$inject = ['$http','$localStorage' ,'API', '$state'];

    function AuthenticationService($http, $localStorage, API, $state) {
        var service = {};
 
        service.Login = Login;
        service.Logout = Logout;
 
        return service;
 
        function Login(userName, callback) {
            $http({
              method: 'POST',
              url: API.url + 'auth',
              withCredentials: true,
              headers: {
                  'Content-Type': 'application/json; charset=utf-8'
              },
              data:{userName: userName}
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }
 
        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $state.transitionTo("login");
            event.preventDefault();
        }
    }
})();