;(function() {

	angular.module("message-play").factory("registerService", registerService);

	registerService.$inject = ['$http','API'];

	function registerService($http,API){

		 function postCreateUser(userName, callback) {

            $http({
              method: 'POST',
              url: API.url + 'user/'+userName,
              withCredentials: true,
              headers: {
                  'Content-Type': 'application/json; charset=utf-8'
              }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

		var service = {
            postCreateUser: postCreateUser
		};

		return service;
	}
})();