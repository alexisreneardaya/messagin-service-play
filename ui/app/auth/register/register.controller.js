;(function() {

  angular.module("message-play").controller('RegisterController', RegisterController);

  RegisterController.$inject = ['$scope', 'resizeTemplate','registerService','$state','status_code','$localStorage'];

  function RegisterController($scope, resizeTemplate,registerService,$state,status_code,$localStorage) {
      resizeTemplate.resize();
      $scope.userTemp="";
      $scope.userName = "";
      $scope.messageAlert="";
      $scope.userCreate=userCreate;

      $scope.goToMessage =function () {
          $state.go("message");
          $('#mymodal').modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
      }
      function userCreate(){
          $scope.alertError=false;
          registerService.postCreateUser($scope.userName, function (result) {
              $scope.userTemp = result;
              if ($scope.userTemp.status == -1) {
                  $scope.messageAlert = "  Problema de conexión";
                  $scope.alertError = true;
                  cleanInputs();
              }
              if ($scope.userTemp.data.code == status_code.CREATE) {
                  $("#mymodal").modal("show");
                  $localStorage.currentUser = {
                      userId: $scope.userTemp.data.data.id,
                      userName: $scope.userTemp.data.data.userName
                  };
                  cleanInputs();
              }
              if($scope.userTemp.data.code == status_code.CONFLICT){
                  $scope.messageAlert="  El usuario ya existe.";
                  $scope.alertError=true;
                  cleanInputs();
              }
              if ($scope.userTemp.data.code == status_code.INTERNAL_SERVER_ERROR) {
                  $scope.messageAlert ="  Problema interno del servidor";
                  $scope.alertError = true;
                  cleanInputs();
              }
              if ($scope.userTemp.status == status_code.INTERNAL_SERVER_ERROR) {
                  $scope.messageAlert="  Problema interno del servidor";
                  $scope.alertError = true;
                  cleanInputs();
              }
          });
      }
      function cleanInputs() {
          try{
              $scope.userName="";
          }catch(e){

          }
      }
  }

})();