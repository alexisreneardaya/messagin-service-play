;(function() {

    angular.module("message-play").factory("createMessageService", createMessageService);

    createMessageService.$inject = ['$http','API','$localStorage'];

    function createMessageService($http,API,$localStorage){

        function postCreateMessages(userDestination,message, callback){
            return $http({
                method: 'POST',
                url: API.url+'message/'+ $localStorage.currentUser.userName+"/"+userDestination+"?msg="+message+"",
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            })
        }

        function getUsers(){
            return $http({
                method: 'GET',
                url: API.url+'user',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            })
        }


        var service = {
            postCreateMessages: postCreateMessages,
            getUsers:getUsers
        };

        return service;
    }

})();
