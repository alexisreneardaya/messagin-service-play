;(function() {

    angular.module("message-play").controller('CreateMessageController', createMessageController);

    createMessageController.$inject = ['$scope', 'resizeTemplate','createMessageService','status_code','$localStorage'];

    function createMessageController($scope, resizeTemplate,createMessageService,status_code,$localStorage) {
        $scope.UserDestination="";
        $scope.messageAlert="";
        $scope.userName=$localStorage.currentUser.userName;
        $scope.mensaje="";
        $scope.resultCreate= [];
        resizeTemplate.resize();

        $scope.createMessage= createMessage;
        function createMessage() {
            $scope.alertError=false;
            $scope.alertSucces=false;
            createMessageService.postCreateMessages($scope.UserDestination,$scope.mensaje, function (result) {
                $scope.resultCreate = result;
                if ($scope.resultCreate.status == -1) {
                    $scope.messageAlert = "  Problema de conexión";
                    $scope.alertError = true;
                    cleanInputs();
                }
                if ($scope.resultCreate.status == status_code.NOT_FOUND) {
                    $scope.messageAlert = "  Seleccione un usuario";
                    $scope.alertError = true;
                    cleanInputs();
                }
                if ($scope.resultCreate.data.code == status_code.CREATE) {
                    $scope.messageAlert="  Mensaje Enviado Correctamente.";
                    $scope.alertSucces=true;
                    cleanInputs();
                }
                if($scope.resultCreate.data.code == status_code.NOT_FOUND){
                    $scope.messageAlert=" El usario de destino no existe.";
                    $scope.alertError=true;
                    cleanInputs();
                }
                if ($scope.resultCreate.data.code == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert ="  Problema interno del servidor";
                    $scope.alertError = true;
                    cleanInputs();
                }
                if ($scope.resultCreate.status == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert="  Problema interno del servidor";
                    $scope.alertError = true;
                    cleanInputs();
                }
            });
        }
        function cleanInputs() {
            try{
                $scope.mensaje="";
                $scope.UserDestination=""
            }catch(e){

            }
        }

    }

})();