;(function() {

	angular.module("message-play").factory("messageService", messageService);

    messageService.$inject = ['$http','API','$localStorage'];

	function messageService($http,API,$localStorage){

		function getUserMessages(){
			return $http({
			  method: 'GET',
			  url: API.url+'message/'+ $localStorage.currentUser.userName,
			  withCredentials: true,
	          headers: {
	          	'Content-Type': 'application/json; charset=utf-8'
	          }
			}).then(function(result) {
			  return result;
			}).catch(function(error) {
			  return error;
			});
		}

		var service = {
            getUserMessages: getUserMessages
		};

		return service;
	}

})();