;(function() {

  angular.module("message-play").controller('MessageController', messageController);

    messageController.$inject = ['$state','$scope', 'resizeTemplate','messageService','$localStorage','AuthenticationService','status_code'];

  function messageController($state,$scope, resizeTemplate,messageService,$localStorage,AuthenticationService,status_code) {
    $scope.itemsMessages = [];
    $scope.messages=[];
    $scope.userName=$localStorage.currentUser.userName;
    $scope.messageAlert="";
    $scope.spinnerMessages=true;
    $scope.itemsMessageSelected = [];
    $scope.messageSelected=[];
    resizeTemplate.resize();

    userMessages();
    function userMessages(){
      $scope.alertError = false;
      var promise = messageService.getUserMessages();
      if(promise){
        promise.then(function (result) {
            try{
                $scope.itemsMessages = result;
                $scope.spinnerMessages=false;
                if ($scope.itemsMessages.status == -1) {
                    $scope.messageAlert = "  Problema de conexión";
                    $scope.alertError = true;
                }
                if ($scope.itemsMessages.data.code == status_code.OK) {
                    $scope.messages = $scope.itemsMessages.data;
                }
                if ($scope.itemsMessages.status ==status_code.NO_CONTENT) {
                    $scope.messageAlert = "  No hay mensajes para mostrar.";
                    $scope.alertError = true;
                }
                if ($scope.itemsMessages.code == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert = "  Problema interno del servidor";
                    $scope.alertError = true;
                }
                if ($scope.itemsMessages.status == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert = "  Problema interno del servidor";
                    $scope.alertError = true;
                }
            }catch (e){
            }
        });
      }
    }

    $scope.logout = logout;
    function logout(){
      AuthenticationService.Logout();
    }
  }

})();