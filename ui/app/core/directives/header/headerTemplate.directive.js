'use strict';

angular.module('message-play')
	.directive('headerTemplate',function(){
		return {
	        templateUrl:'app/core/directives/header/header.html',
	        restrict: 'A',
	        replace: false
    	}
	});

