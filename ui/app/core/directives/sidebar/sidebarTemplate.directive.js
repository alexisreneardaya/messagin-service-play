'use strict';

angular.module('message-play')
	.directive('sidebarTemplate',function(){
		return {
	        templateUrl:'app/core/directives/sidebar/sidebar.html',
	        restrict: 'A',
	        replace: false
    	}
	});

