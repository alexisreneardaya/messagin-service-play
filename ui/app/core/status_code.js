;(function() {

    angular.module("message-play").constant('status_code', {
        'OK': '200',
        'CREATE': '201',
        'NO_CONTENT': '204',
        'FOUND':'302',
        'NOT_MODIFIED': '304',
        'UNAUTHORIZED': '401',
        'FORBIDDEN': '403',
        'NOT_FOUND': '404',
        'CONFLICT': '409',
        'INTERNAL_SERVER_ERROR': '500',
    });

})();
