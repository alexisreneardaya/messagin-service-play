;(function() {
  'use strict';

  angular.module("message-play",['ui.router', 'ngStorage']);

  /**
   * Definition of the main app module and its dependencies
   */
  angular.module("message-play").config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$stateProvider','$urlRouterProvider', '$httpProvider'];

  function config($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;

    $urlRouterProvider.otherwise('/');

    // routes
    $stateProvider
        .state('main', {
            abstract: true,
            templateUrl:'app/core/pages/main.html',
            data : {
              cssClassnames : 'hold-transition skin-red sidebar-mini layout-boxed'
            }
        })
        .state('login', {
            url:'/login',
            controller: 'LoginController',
            templateUrl:'app/auth/login.html',
            data : {
                cssClassnames : 'hold-transition login-page'
            }
        })
        .state('register', {
            url:'/register',
            controller: 'RegisterController',
            templateUrl:'app/auth/register/register.html',
            data : {
                cssClassnames : 'hold-transition register-page'
            }
        })
        .state('message', {
            url:'/',
            parent: 'main',
            controller: 'MessageController',
            templateUrl:'app/userMessage/userMessage.html',
        })
        .state('createMessage', {
            url:'/create',
            parent: 'main',
            controller: 'CreateMessageController',
            templateUrl:'app/createMessage/createMessage.html',
        })

  }

  angular.module("message-play").run(run);

  run.$inject = ['$rootScope', '$http', '$location', '$state', '$localStorage'];

  function run($rootScope, $http, $location, $state, $localStorage) {
      if (localStorage.getItem("currentUser") === null) {
          $state.transitionTo("login");
      }

      $rootScope.$on('$locationChangeStart', function (event, next, current) {
          var publicPages = ['/login','/register'];

          var restrictedPage = publicPages.indexOf($location.path()) === -1;

          if (restrictedPage && !$localStorage.currentUser) {
              $state.transitionTo("login");
              event.preventDefault();
          }
      });

  }

})();