# **MESSAGES SERVICE-PLAY**

## **Introduction**
Exercise: [Messages list service](https://gist.github.com/timoteoponce/5a563442025769f8b47e5e319ba5bb3a)

This exercise was maked with the PLAY WEB FRAMEWORK which has REST services about User management and Messages management. 

## **Prerequisites**
For this aplication, it is important to know, how play web framework work,so i suggest you to see this documentation:
[PLAY WEB FRAMWORK](https://www.playframework.com/documentation/2.5.x/Home)

To run the Play framework, you need JDK 6 or later.

Be sure to have the java and javac commands in the current path (you can check this by typing java -version and javac -version at the shell prompt).

These are some tools that you must have to start this proyect:
* A favorite IDE or text editor. I suggest you [Intellij IDEA](https://www.jetbrains.com/idea/). 
* Setting up your preferred IDE: [link](https://www.playframework.com/documentation/2.5.x/IDE)

## **Download the binary package**
Download the [Play 2.0 binary package](http://download.playframework.org/releases/play-2.0.zip) and extract the archive to a location where you have both read and write access.

## **Add the play script to your PATH**

On Windows you’ll need to set it in the global environment variables.

## **Check that the play command is available**
From a shell, launch the play help command.
```
$ play help
```
If everything is properly installed, you should see the basic help.

## **Build the proyect**

The easiest way to start an application in production mode is to use the start command from the Play console. 
This requires a Play 2.0 installation on the server.
```
$ play start
```

## **REST services**

###User

#####Get All Users
```
 [GET] http://localhost:9000/user
```
#####Create a User
```
 [POST] http://localhost:9000/user/:userName
```
#####Update User
```
 [PUT] http://localhost:9000/user/:id/:userName/:state
```
#####Delete a User
```
 [DELETE] http://localhost:9000/user/:id 
```
#####Authentication of a User
```
 [POST] http://localhost:9000/auth 
```

### Message

#####Get all messages in general
```
 [GET] http://localhost:9000/message
```
#####Get messages of a User
```
 [GET] http://localhost:9000/message/:userName
```
#####Send message between Users
```
 [POST] http://localhost:9000/message/:userOrigin/:userDestination}?msg={message}
```

###Example response
```Json
{
  "code": 404,
  "status": "success",
  "message": "User not found.",
  "data":[]
}
```
**Explanation data result of endpoints**

* code: Response error code
* status: Status depending on error code
* message: Message depending on operation or error
* data: Response data, can be unique result or array result

## **UI of this proyet**

In the repository there is a UI folder based in AngularJS with a bootstrap template. You can see all endpoints in action.